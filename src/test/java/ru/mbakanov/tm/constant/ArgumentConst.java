package ru.mbakanov.tm.constant;

public interface ArgumentConst {

    String HELP = "-h";

    String ABOUT = "-a";

    String VERSION = "-v";

    String INFO = "-i";

    String ARGUMENTS = "-arg";

    String COMMANDS = "-cmd";

}
