package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.ICommandController;
import ru.mbakanov.tm.api.ICommandService;
import ru.mbakanov.tm.constant.TerminalConst;
import ru.mbakanov.tm.model.TerminalCommand;
import ru.mbakanov.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = commandService.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println(TerminalConst.VERSION_NUM);
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println(TerminalConst.DEV_NAME);
        System.out.println(TerminalConst.DEV_EMAIL);
    }

    public void showUnexpected(final String arg) {
        StringBuilder result = new StringBuilder();
        result.append("['").append(arg).append("' - unexpected command]");
        System.out.println(result);
    }

    public void showInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void showArguments() {
        final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    public void exit() {
        System.exit(0);
    }

}
