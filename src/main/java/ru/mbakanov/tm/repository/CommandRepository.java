package ru.mbakanov.tm.repository;

import ru.mbakanov.tm.api.ICommandRepository;
import ru.mbakanov.tm.constant.ArgumentConst;
import ru.mbakanov.tm.constant.CommandConst;
import ru.mbakanov.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            CommandConst.HELP, ArgumentConst.HELP, "Show terminal`s commands."
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            CommandConst.VERSION, ArgumentConst.VERSION, "Show application version."
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            CommandConst.INFO, ArgumentConst.INFO, "Show system hardware info."
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            CommandConst.EXIT, null ,"Close application."
    );

    public static final TerminalCommand COMMAND = new TerminalCommand(
            CommandConst.COMMANDS, ArgumentConst.COMMANDS ,"Show program commands."
    );

    public static final TerminalCommand ARGUMENT = new TerminalCommand(
            CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS ,"Show program arguments."
    );

    private static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[] {
            HELP, ABOUT, VERSION, INFO, COMMAND, ARGUMENT, EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands (TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs (TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
