package ru.mbakanov.tm.api;

import ru.mbakanov.tm.model.TerminalCommand;

public interface ICommandService {

    String[] getCommands();

    String[] getArgs();

    TerminalCommand[] getTerminalCommands();

}
