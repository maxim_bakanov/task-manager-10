package ru.mbakanov.tm.api;

import ru.mbakanov.tm.model.TerminalCommand;

public interface ICommandRepository {

    String[] getCommands (TerminalCommand... values);

    String[] getArgs (TerminalCommand... values);

    String[] getCommands();

    String[] getArgs();

    TerminalCommand[] getTerminalCommands();

}
