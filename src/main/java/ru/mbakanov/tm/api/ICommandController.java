package ru.mbakanov.tm.api;

public interface ICommandController {

    void showHelp();

    void showVersion();

    void showAbout();

    void showUnexpected(final String arg);

    void showInfo();

    void showCommands();

    void showArguments();

    void exit();

}
