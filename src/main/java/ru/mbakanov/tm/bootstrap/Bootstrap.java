package ru.mbakanov.tm.bootstrap;

import ru.mbakanov.tm.api.ICommandController;
import ru.mbakanov.tm.api.ICommandRepository;
import ru.mbakanov.tm.api.ICommandService;
import ru.mbakanov.tm.constant.ArgumentConst;
import ru.mbakanov.tm.constant.CommandConst;
import ru.mbakanov.tm.repository.CommandRepository;
import ru.mbakanov.tm.service.CommandService;
import ru.mbakanov.tm.controller.CommandController;

import java.util.Scanner;

public final class Bootstrap {

    public final ICommandRepository commandRepository = new CommandRepository();

    public final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        System.out.println("== Welcome to TASK MANAGER ==");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.EXIT:
                commandController.exit();
                break;
            default:
                commandController.showUnexpected(arg);
                break;
        }
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showUnexpected(arg);
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
